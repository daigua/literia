// JavaScript Document
(function(){
	
	var Window = function(node){
		this.node = node;
		this.EventCenter = {};
	};
	var P = Window.prototype;
	var W;
	
	var SLICE = window.SLICE = Array.prototype.slice;
	/**
		由当前Window对象获取新的Window对象
	*/
	P.w = function(node){	
		var w;
		if(typeof node == "string")
			node = this.$(node);
		if(!node.data("W")){
			w = new Window(node);
			w.parent = this;
			node.data("W",w);
		}else{
			w = node.data("W");
		}
		return w;
	}
	/**
		获取本窗口中的jquery对象
	*/
	P.$ = function(node){
		return this.node.find(node);
	}
	/**
		
	*/
	/**
		窗口中打开页面,通过ajax请求打开
	*/
	P.show = function(url,options){
		options = $.extend({
			type : "get",
			data : {}
		}, options || {});
		var _this = this;	
		var _callback = function(html){		
			var clip = Response(html).clip;
			_this.node.html(clip.html);				
			var script = clip.script;
			clip.meta = clip.meta || {};
			/**
				复制当前的W对象,以避免多次打开窗口形成W对象污染
			*/
			var w = {};
			script = " var W = this;\n\r" + script;
			$.extend(w, _this);
			var argsStr = "";
			if(clip.meta && clip.meta.args){
				$.each(clip.meta.args.split(","),function(i,n){
					if(argsStr != "")
						argsStr += ",";
					argsStr += "\""+n+"\"";
				});	
			}
			var fn = new Function(script);	
			if(argsStr != ""){
				fn = eval("new Function("+argsStr+",script)");
			}
			_this.render(w);
			if(argsStr != ""){
				fn.apply(w, options.args || []);
			}else{
				fn.call(w);	
			}
			
			_this.meta = clip.meta;		
			if(options.done)
				options.done({meta:clip.meta,node:_this.node});
		}
		Request[options.type](url,options.data,_callback,"html");
	}
	P.fire = function(){
		var key = arguments[0];
		var args = SLICE.call(arguments,1);
		var _this = this;
		if(this.EventCenter[key]){
			$.each(this.EventCenter[key],function(i,fn){
				fn.apply(_this,args);
			})
		}
	}
	P.on = function(key,fn){
		if(this.EventCenter[key]){
			this.EventCenter[key].push(fn);
		}else{
			this.EventCenter[key] = [fn];			
		}
	}
	P.hide = function(){
		
	}
	P.clear = function(){
		this.EventCenter = {};
		this.node.empty();	
	}
	P.destroy = function(){
		delete this;
		this.node.remove();
	}
	/**
		处理模板
	*/
	P.template = function(data,W,fn){	
		var source = this.node.data("template-source");
		if(!source){
			source = this.node.find(".template-source").val();
			if(!source)
				return
			this.node.data("template-source",source);
			this.node.empty();
		}
		var out = Template.process(source,data);
		if($.trim(out) == "")
			return;
		out = $(out);
		if(!fn)
			this.node.empty();		
		out = $("<div />").append(out);	
		if(W){
			Widget.render(out,W);
		}else{
			Widget.render(out);
		}
		this.node.append(out.children());
	}
	P.render = function(W){
		if(!W)
			W = this;
		Widget.render(this.node,W);
	}
	P.nodeName = function(){
		elem = elem || this.node[0];
		return elem.nodeName && elem.nodeName.toLowerCase();
	}
	P.data = function(data){
		if(this.node.is("form")){
			this.node.val(data);
		}else{
			this.node.html(data);
		}
	}
	
	$(function(){
		W = window.W = new Window($(document.body));
		W.parent = window;
		W.render();
	});

var Utils = {
	nodeName : function(elem){
		elem = elem || this.node[0];
		return elem.nodeName && elem.nodeName.toLowerCase();
	}
};

/**
	封装响应
*/
var Response = (function(){	
	var parseToClip = (function(){	
		var metaReg = /<clip\-meta>([\s\S]*?)<\/clip-meta>/gim;
		var htmlReg = /<clip\-html>([\s\S]*?)<\/clip\-html>/gim;
		var scriptReg = /<script.*?>([\s\S]*?)<\/script>/gim;
		return function(clipHtml){
			var meta = clipHtml.match(metaReg);
			if(meta){
				meta = meta[meta.length-1];
				meta = meta.replace(metaReg,"$1");
				meta = evalJSON(meta);
			}
			var html = clipHtml.match(htmlReg);
			if(html){
				html = html.join(" ");
				html = html.replace(htmlReg,"$1");
			}
			var script = clipHtml.match(scriptReg);
			if(script){
				script = script.join(" ");
				script = script.replace(scriptReg,"$1");
			}
			return {
						meta : meta,
						html : html,
						script : script
					};
		};
	})();
	
	return function(html){
		return {
			clip : parseToClip(html)
		};
	}
		
})();

/**
		Widgets
	*/
	var Widget = window.Widget = (function(){
		
		var widgets = {};//被动渲染器
		var initiativeWidgets = {}; //主动渲染器
		
		return {
			extend : function(widget){
				if(!widget)
					return;
				$.each(widget,function(name,fn){
					widgets[name] = fn;
				});
			},
			initiative :{
				extend : function(widget){
					if(!widget)
						return;
					$.each(widget,function(name,fn){
						initiativeWidgets[name] = fn;
					});
				}
			},
			render : function(node,W){				
				//主动渲染
				$.each(initiativeWidgets,function(sel,fn){
					if(W)
						fn.call(W,node.find(sel));
					else
						fn(node.find(sel));
				});
				//被动渲染
				var renderNodes = node.find("*[render]");
				$.each(renderNodes,function(i,el){
					el = $(el);
					var rs = el.attr("render").split(",");					
					for(var j = 0; j < rs.length; j++){
						var re = rs[j];
						if(widgets[re]){
							if(W)
								widgets[re].call(W,el);
							else
								widgets[re](el);
						}
					}	
				});
			}
		};
	})();
	var Request = window.Request = (function(){
		
		var Interceptors = {
			before : [],
			after : []	
		};
		var interceptor = {
			before : function(req){
				var flag = true;
				for(var i = 0; i < Interceptors.before.length; i++){
					if(!Interceptors.before[i](req))
						flag = false;
				}
				return flag;
			},
			after : function(req,data){
				var flag = true;
				for(var i = 0; i < Interceptors.after.length; i++){
						if(!Interceptors.after[i](req,data))
							flag = false;
				}
				return flag;
			}
		};
		var ajaxOption = {
			cache : false,
			data : {},
			dataType : "json",
			success : function(){},
			type : "GET",
			url  : "",
			async : true		
		};
		var proxy = function(req){
			if(!interceptor.before(req))
				return;
			var setting = {};
			$.extend(setting, ajaxOption, req || {});
			setting.success = (function(success){
				return function(data){
					UI.Notify.show("加载完成").stick(300);
					if(!interceptor.after(req,data))
						return;
					setTimeout(function(){
						success(data);	
					},0);
				}	
			})(setting.success);
			UI.Notify.show("正在加载，请稍候……");
			$.ajax(setting);
		};
		return {
			/**
				{
					before : function(url){},
					after : function(data){}
				}
			*/
			interceptor : function(){
				$.each(arguments,function(i,interceptor){
					$.each(interceptor,function(key,inter){
						Interceptors[key].push(inter);	
					});	
				});
			},
			/**
				代理进行ajax请求
			*/
			get : function(url,data,callback,dataType){
				dataType = dataType || "json";
				var req = {
					url : url,
					data : data,
					success : callback,
					dataType : dataType,
					type : "GET"	
				};
				proxy(req);
			},
			post : function(url,data,callback,dataType){
				dataType = dataType || "json";
				var req = {
					url : url,
					data : data,
					success : callback,
					dataType : dataType,
					type : "POST"	
				};
				proxy(req);
			}
		};
	})();

	/**
		事件,这里不包括浏览器的原生事件,浏览器的原生事件由浏览器处理
	*/
	var Event = function(type){
		this.type = type;
	};

/**
	方法装饰,为方法添加额外的功能.
*/
(function(){
	var decorates = {};
	Function.decorate = function(decorate){
		$.each(decorate,function(key,fn){
			decorates[key] = fn;
			Function.prototype[key] = function(){
				var _this = this;
				var args = SLICE.call(arguments);
				return function(){
					arguments = SLICE.call(arguments);				
					decorates[key](_this,args,arguments);
					_this.apply(_this,arguments);
				}
			};
		});
	}
})();
	
/**
	javascript simple template engine
	
	template like
	var datas = [1,2,3,4,5];
	var html = Template.process({datas:datas});
	<+ 
		for(var i = 1; i < datas.length; i++){ 
			var data = datas[i];
	+>
		<+=data.name+>
	<+ }+>
	
*/
var Template = window.Template = (function(){
	var splitReg = /<\+[\s\S]*?\+>/gi;	
	var scriptReg = /<\+([\s\S]*?)\+>/gi;
	var quoteReg = /"/gim;
	var replaceQuote = function(str){
		return str.replace(quoteReg,"\\\"");
	}
	var compile = function(source){
		//source = " "+source.replace(/[\n\r]/g," ")+" ";
		var fn_str = " with(this){ var out = ''; ";
		var strs = source.split(splitReg);
		var scripts = source.match(splitReg);
		var outs = [];
		for(var i = 0; i < strs.length; i++){
			outs.push(" out += \""+replaceQuote(strs[i])+"\"; ");
			if(scripts && scripts.length > i){
				var scriptStr = scripts[i];
				scriptStr = scriptStr.replace(scriptReg,"$1");				
				if(/^=.*$/.test(scriptStr)){					
					outs.push(" out += "+scriptStr.substring("1")+";");
				}else{
					outs.push(scriptStr);
				}
			}
			
		}
		fn_str += outs.join("");
		fn_str += "return out;}";
		return fn_str;
	}
	
	var cache = {};
	
	return {
		process : function(source,data){
			source = source.replace(/[\n\r]/g," ")+" ";
			source_key = source.replace(/\s/g,"");
			var fn = cache[source_key];
			if(!fn){
				var fn_str = compile(source);
				fn = new Function(fn_str);
				cache[source_key] = fn;
			}			
			return fn.call(data); 
		}
	}
})();

/**
	处理分页信息
*/
var paging = window.paging = function(page, pageSize, totalCount, showSize){
	showSize = showSize || 10;
	var totalPage = Math.ceil(totalCount/pageSize);
	var startPage = page - Math.floor(showSize/2);
	if(startPage < 1)
		startPage = 1;
	var endPage = startPage + showSize - 1;
	if(endPage > totalPage){
		endPage = totalPage;
		startPage = endPage - showSize;
		if(startPage < 1)
			startPage = 1;
	}
	var prePage = 0;
	if(startPage > 1)
		prePage = startPage - 1;
	var nextPage = 0;
	if(endPage < totalPage)
		nextPage = endPage + 1;
	var startNum = (page - 1) * pageSize + 1;
	var endNum = startNum + pageSize - 1;
	if(endNum > totalCount)
		endNum = totalCount;
	return {
		page : page,
		pageSize : pageSize,
		totalCount : totalCount,
		startPage : startPage,
		endPage : endPage,
		startNum : startNum,
		endNum : endNum,
		prePage : prePage,
		nextPage : nextPage
	}
}

})();