// JavaScript Document
(function(){
	Request.interceptor(
		(function(){//url锁定,相同url值允许访问一次
			var lock = {};
			return {
				before : function(req){					
					if(lock[req.url])
						return false;
					lock[req.url] = 1;
					return true;
				},
				after :	function(req,data){
					delete lock[req.url];
					return true;
				}
			}	
		})(),
		//登录超时拦截器
		{
			after :	function(req,data){
					if(data.loginExpire){
						window.location.href='http://container.api.taobao.com/container?appkey=12264547&encode=utf-8';
						return false;
					}
					return true;
				}	
		},
		//异常拦截器
		{
			after : function(req,data){//错误拦截器提示
					if(data.exception){
						alert("操作失败,原因是"+data.exception.message);
						return false;
					}
					return true;
				}	
		},
		//收费功能拦截器
		{
			after : function(req,data){//错误拦截器提示
					if(data.noAuth){
						UI.Panel.showUrl(contextPath+'/order_tip.jsp');
						return true;
					}
					return true;
				}	
		}
	
	);	
})();