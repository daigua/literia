// JavaScript Document
Widget.extend({
	"data-click" : function(node){
		var W = this;
		node = $(node);
		var script = node.attr("data-click");
		if(!script)
			return;
		var fn = new Function("W","e","node",script);
		node.click(function(e){fn.call(node,W,e,node)});
	},
	"data-change" : function(node){
		var W = this;
		node = $(node);
		var script = node.attr("data-change");
		if(!script)
			return;
		var fn = new Function("W","e",script);
		node.change(function(e){fn.call(node,W,e)});
	},
	"data-blur" : function(node){
		var W = this;
		node = $(node);
		var script = node.attr("data-blur");
		if(!script)
			return;
		var fn = new Function("W","e",script);
		node.blur(function(e){fn.call(node,W,e)});
	},
	"search" : function(node){
		var W = this;
		node = $(node);
		var nodeW = W.w(node);
		var searchTimeout;
		var placeholder = node.attr("placeholder");
		var script = node.attr("data-search");
		if(script){
			script = new Function("W","node","keyword",script);
		}
		node.keyup(function(){
			if(searchTimeout){
				window.clearTimeout(searchTimeout);
			}
			searchTimeout = window.setTimeout(function(){
				searchTimeout = 0;
				nodeW.fire("search",getVal());
				if(script)
					script.call(node,W,node,getVal());
			},200);			 
		});
		var getVal = function(){
			var val = node.val();
			if(val == placeholder){
				return "";
			}
			return val;
		}
	},
	"list-row" : function(node){
		node.hover(
			function(){
				$(this).addClass("mouseover");	
			},
			function(){
				$(this).removeClass("mouseover");	
			}
		).bind("select-row",function(){
			$(this).addClass("selected");	
		}).bind("unselect-row",function(){
			$(this).removeClass("selected");	
		});
	},
	"checkbox-select-all" : function(node){
		var _this = this;		
		node.click(function(){
			var checkboxes = _this.$(node.attr("checkbox-select-all")).find(":checkbox");
			checkboxes.attr("checked",node.attr("checked"));
		});
	},
	"row-select-handle" : function(node){	
		node.change(function(){
			var row = node.closest(".list-row");
			if($(this).attr("checked")){				
				row.trigger("select-row");	
			}else{				
				row.trigger("unselect-row");
			}
		});
	},
	"input-text-select" : function(node){
		node.focus(function(){
			$(this).select();	
		})
	},
	'datepicker' : function(node){
		var optStr = node.attr("datepicker");
		var opt = {};
		if(optStr){
			opt = evalJSON(opt);
		}
		opt = $.extend({
			showAnim : '',
			onSelect : function(){
				node.blur();	
			}	
		},opt);
		node.datepicker(opt);
	},
	"template" : function(node){		
		var source = node.data("template-source");
		if(!source){
			source = node.find(".template-source").val();
			if(!source)
				return
			node.data("template-source",source);
		}
		var W = this.w(node);
		var _this = this;
		
		function process(data){
			var out = Template.process(source,data);
			if($.trim(out) == "")
				return;
			out = $(out);
			out = $("<div />").append(out);	
			Widget.render(out,_this);
			out = out.children();
			return out;
		}
		
		
		
		W.template = {
			data : function(data){
				node.empty();
				node.append(process(data));
			},
			append : function(data){
				node.find(".template-tip-content").remove();
				node.append(process(data));
			},
			prepend: function(data){
				node.find(".template-tip-content").remove();
				node.prepend(process(data));
			},
			empty : function(){
				node.empty();
			},
			update : function(out,data){
				out.replaceWith(process(data));
			}
		};
	
	},
	"colortip" : function(node){
		var color = node.attr("colortip");
		color = color || "green";
		node.colorTip({color:color});
	},
	"form-select" : function(node){
		var W = this.w(node);
		var getOption = function(val){
			return node.find("[value='"+val+"']");
		};
		var currentOption = function(){
			return node.find("[value='"+node.val()+"']");
		}
		var addOptions = function(options){
			$.each(options, function(i,option){
				node.append("<option value='"+option.value+"'>"+option.text+"</option>");
			})
		}
		var addOption = function(option){
			addOptions([option]);
		}
		W.form_select = {
			option : getOption,
			currentOption : currentOption,
			add : addOption
		}
	},
	"poshytip" : (function(){
		var defaultOptions = {
					className: 'tip-green',
					showTimeout: 1,
					alignTo: 'target',
					alignX: 'center',
					alignY: 'bottom',
					offsetY: 5,
					allowTipHover: false
		};
		return function(node){
			var options = node.attr("poshytip");	
			options = $.extend(defaultOptions, options || {});		
			node.poshytip();	
		}	
	})(),
	"tabs" : function(node){		
		var tabs = node.attr("tabs");
		if(tabs)
		tabs = evalJSON(tabs);
		tabs = $.extend({tabs:".tabs-tabs",panels:".tabs-panels > div",current:"current"}, tabs || {});
		var panels = node.find(tabs.panels);
		$(node.find(tabs.tabs)).tabs(panels); 
	},
	placeholder : function(node){
		var tip = node.attr("placeholder");
		if(!tip)
			return;
		if(node.val() == "")
			node.val(tip).addClass("placeholder");
		node.focus(function(){
			if(node.val() == tip)
				node.val("").removeClass("placeholder");	
		});
		node.blur(function(){
			if($.trim(node.val()) == "")
				node.val(tip).addClass("placeholder");	
			else
				node.removeClass("placeholder");	
		});
	},
	"data-bind" : (function(){//只能绑定当前W的变量的值。	
		
		var refresh = (function(){			
			var refreshNodes = function(dataBinder,nodes,val){
				$.each(nodes,function(i,node){
					var W = dataBinder.W.w(node);
					W.data(val);
				});
			}
			
			return function(dataBinder,path){				
				if(path){
					var val = getValueByPath(dataBinder.W,path);
					var nodes = dataBinder.bindedNodes(path);
					refreshNodes(dataBinder,nodes,val);
				}else{
					dataBinds = dataBinder.dataBinds;
					$.each(dataBinds,function(path,nodes){
						var val = getValueByPath(dataBinder.W,path);
						refreshNodes(dataBinder,nodes,val);
					});
				}
			}
		})();
		
		var getValueByPath = function(W,path){	
			return (new Function(" return this."+path)).call(W);
		}
		//根据W对象获取当前页面的data-bind Pathes。
		
		function DataBinder(W){
			this.W = W;
			this.dataBinds = {};
			this.init();
		}
		var P = DataBinder.prototype;
		P.bindPath = function(path,node){
			if(!this.dataBinds[path]){
				this.dataBinds[path] = [];
			}
			this.dataBinds[path].push(node);
		}
		P.bindedNodes = function(path){
			return this.dataBinds[path];
		}
		P.refresh = function(path){
			refresh(this,path);
		}
		P.init = function(){
			this.W.dataBinder = this;
		}
		
		var getDataBinder = function(W){
			var dataBinder ;
			if(W.dataBinder){
				dataBinder = W.dataBinder;
			}else{
				dataBinder = new DataBinder(W);
			}
			return dataBinder;
		}
		
		return function(node){
			var W = this, dataPath = node.attr('data-bind');
			if(!dataPath)
				return;
			var dataBinder = getDataBinder(W);
			dataBinder.bindPath(dataPath,node);
		}
	})(),
	"checkbox" : (function(){
		var setting = {empty:contextPath+"/images/empty.png"};
		return function(node){
			$(node).checkbox(setting);
			$(node).bind("check uncheck disable enable",function(){
				$(this).trigger("change");	
			});			
		}
	})(),
	"popmenu" : function(node){
		var W = this;
		var opt = node.attr("popmenu");
		if(!opt)
			return;
		opt = evalJSON(opt);
		opt.menuSelector = W.$(opt.menuSelector);
		node.popmenu(opt);
	},
	"colorbox" : function(node){
		var conf = node.attr("colorbox");
		if(conf){
			conf = $.toJSON(conf);
		}else
			conf = {};
		$(node).colorbox(conf);
	}
});
Widget.initiative.extend({
	"form" : function(node){
		$(node).submit(function(){
			node.find(".form-submit").click();
			return false;
		});
	}
});